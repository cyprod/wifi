package com.cs.mines.wifimodel;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

import java.util.ArrayList;

public class Compass implements SensorEventListener{
    private static Compass sCompass = null;
    private SensorManager mSM;
    private ArrayList<CompassCallback> mReceivers;

    public static synchronized Compass getCompass(Context context) {
        if(sCompass == null) {
            sCompass = new Compass(context);
        }
        return sCompass;
    }

    public Compass(Context context) {
        mReceivers = new ArrayList<CompassCallback>();
        mSM = (SensorManager)context.getSystemService(Context.SENSOR_SERVICE);
    }

    private void pauseCompass() {
        mSM.unregisterListener(this);
    }

    private void resumeCompass() {
        Sensor gSensor = mSM.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        Sensor mSensor = mSM.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
        mSM.registerListener(this, gSensor, SensorManager.SENSOR_DELAY_NORMAL);
        mSM.registerListener(this, mSensor, SensorManager.SENSOR_DELAY_NORMAL);
    }

    public synchronized void registerListener(CompassCallback callback) {
        if(!mReceivers.contains(callback)) {
            mReceivers.add(callback);
        }
        if(!mReceivers.isEmpty()) {
            resumeCompass();
        }
    }

    public synchronized void unregisterListener(CompassCallback callback) {
        mReceivers.remove(callback);
        if(mReceivers.isEmpty()) {
            pauseCompass();
        }
    }

    //variables used for calculating direction info
    private float[] mGData = new float[3];
    private float[] mMdata = new float[3];
    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        float[] data;
        float[] R = new float[16];
        float[] I = new float[16];
        float[] orientation = new float[3];
        switch(sensorEvent.sensor.getType()) {
            case Sensor.TYPE_ACCELEROMETER:
                data = mGData;
                break;
            case Sensor.TYPE_MAGNETIC_FIELD:
                data = mMdata;
                break;
            default:
                return;
        }
        System.arraycopy(sensorEvent.values, 0, data, 0, data.length);

        SensorManager.getRotationMatrix(R, I, mGData, mMdata);
        SensorManager.getOrientation(R, orientation);
        double retVal = Math.toDegrees((double)orientation[0]);
        for(CompassCallback callback : mReceivers) {
            callback.getDirection(retVal);
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }
}
