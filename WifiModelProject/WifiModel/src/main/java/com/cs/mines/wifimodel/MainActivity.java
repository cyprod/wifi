package com.cs.mines.wifimodel;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends Activity {
    private AlertDialog mAlert;
    private int mNumFloors;
    private String mTableName;
    private int mBuildingId;
    private boolean mNextScreenLaunched;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        (findViewById(R.id.butCreate)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mNextScreenLaunched = false;
                String floors = ((EditText) findViewById(R.id.numFloors)).getText().toString();
                mTableName = ((EditText) findViewById(R.id.dbName)).getText().toString();

                mTableName = mTableName.toUpperCase();
                if (mTableName.length() > 0) {
                    String whereClause = Contract.DB_FIELD_BUILDING_NAME + "=" + "'" + mTableName +"'";
                    Cursor cursor =
                        getContentResolver().query(Contract.BUILDING_INFO_URI, null, whereClause, null, null);
                    //create building
                    if (cursor == null || cursor.getCount() == 0) {
                        if(cursor != null) {
                            cursor.close();
                        }
                        try {
                            mNumFloors = Integer.parseInt(floors);
                        } catch (NumberFormatException e) {
                            Toast.makeText(MainActivity.this, getResources().getString(R.string.need_floors), Toast.LENGTH_LONG).show();
                            return;
                        }
                        if (mNumFloors < 1) {
                            Utils.displayInvalidParams(MainActivity.this);
                        } else {
                            ContentValues cv = new ContentValues();
                            cv.put(Contract.DB_FIELD_BUILDING_NAME, mTableName);
                            cv.put(Contract.DB_FIELD_NUM_FLOORS, mNumFloors);
                            Uri uri = getContentResolver().insert(Contract.BUILDING_INFO_URI, cv);
                            if(uri != null) {
                                mBuildingId = Integer.parseInt(uri.getLastPathSegment());
                            } else {
                                mBuildingId = -1;
                            }
                            loadNextScreen();
                        }
                    } else {
                        cursor.moveToFirst();
                        mBuildingId = cursor.getInt(cursor.getColumnIndex(Contract.DB_FIELD_ID));
                        cursor.close();
                        displayUseDialog();
                    }
                } else {
                    displayOkAlert(R.string.invalidFile, R.string.fileNotValid);
                }
            }
        });

        WifiManager wifi = (WifiManager)getSystemService(Context.WIFI_SERVICE);
        if(!wifi.isWifiEnabled()) {
            displayOkAlert(R.string.wifi_disabled_title, R.string.wifi_disabled);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(mNextScreenLaunched) {
            finish();
        }
    }

    private void loadNextScreen() {
        mNextScreenLaunched = true;
        Intent nextScreen = new Intent(this, FloorResourcesActivity.class);
        nextScreen.putExtra(FloorResourcesActivity.NUM_FLOORS, mNumFloors);
        nextScreen.putExtra(FloorResourcesActivity.BUILDING_ID, mBuildingId);
        startActivity(nextScreen);
    }

    private void loadDatabase() {
        Cursor cursor = getContentResolver().query(Uri.parse(Contract.BUILDING_INFO_URI.toString() + "/" + mBuildingId), null, null, null, null);
        if(cursor != null) {
            cursor.moveToFirst();
            mNumFloors = cursor.getInt(cursor.getColumnIndex(Contract.DB_FIELD_NUM_FLOORS));
            cursor.close();
            loadNextScreen();
        } else {
            displayOkAlert(R.string.invalidFile, R.string.fileNotValid);
        }
    }

    private void displayOkAlert(int titleId, int messageId) {
        AlertDialog.Builder ab = new AlertDialog.Builder(this);
        ab.setTitle(titleId);
        ab.setMessage(messageId);
        ab.setCancelable(false);
        ab.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                mAlert.dismiss();
            }
        });
        mAlert = ab.show();
    }

    private boolean displayUseDialog() {
        AlertDialog.Builder ab = new AlertDialog.Builder(this);
        ab.setTitle(R.string.useDB);
        ab.setMessage(String.format(getResources().getString(R.string.useAnyway), mTableName));
        ab.setCancelable(true);
        ab.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                mAlert.dismiss();
            }
        });
        ab.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                loadDatabase();
            }
        });
        mAlert = ab.show();
        return false;
    }
 }
