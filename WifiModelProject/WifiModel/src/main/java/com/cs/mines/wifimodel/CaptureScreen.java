package com.cs.mines.wifimodel;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import java.util.List;

public class CaptureScreen extends Activity implements CompassCallback{
    public static final String LOCATION_ID = "location_id";
    private Boolean mNorthDone, mSouthDone, mEastDone, mWestDone;
    private int mCurrentCaptureDirection, mCurrentDirection, mLocationId;
    private AlertDialog mAlert;
    private int mScansReceived;
    private WifiManager mWifi;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_capture_screen);

        Intent launchingIntent = getIntent();
        mLocationId = launchingIntent.getIntExtra(LOCATION_ID, -1);
        if(mLocationId < 0) {
            throw new IllegalArgumentException("Attempting to launch capture without location ID");
        }
        mNorthDone = mSouthDone = mEastDone = mWestDone = false;
        mCurrentDirection = Contract.NORTH;
        loadNextDirection();
        mWifi = (WifiManager)getSystemService(Context.WIFI_SERVICE);

        (findViewById(R.id.butCapture2)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                captureDataPoint();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        Compass.getCompass(this).registerListener(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        Compass.getCompass(this).unregisterListener(this);
    }

    @Override
    public void getDirection(double direction) {
        mCurrentDirection = Utils.setDirectionText(direction, (TextView)findViewById(R.id.textDir2), this);
    }

    private void loadNextDirection() {
        if(!mNorthDone) {
            mCurrentCaptureDirection = Contract.NORTH;
        } else if(!mEastDone) {
            mCurrentCaptureDirection = Contract.EAST;
        } else if(!mSouthDone) {
            mCurrentCaptureDirection = Contract.SOUTH;
        } else if(!mWestDone) {
            mCurrentCaptureDirection = Contract.WEST;
        } else {
            finish();
        }
        updateRequestedDirection();
    }

    private void displayError(int titleId, int messageId) {
        AlertDialog.Builder ab = new AlertDialog.Builder(this);
        ab.setTitle(titleId);
        ab.setMessage(messageId);
        ab.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                mAlert.dismiss();
            }
        });
        ab.setCancelable(false);
        mAlert = ab.show();
    }

    private void createOrUpdateRSSI(ScanResult result) {
        int accessPointId;
        String whereClause = Contract.DB_FIELD_ACCESS_POINT_ADDR + "=" + "'" + result.BSSID + "'";
        Cursor cursor = getContentResolver().query(Contract.ACCESS_POINT_URI, null, whereClause, null, null);
        ContentValues cv;
        //get the access point id if it exists, otherwise, create it
        if(cursor == null || cursor.getCount() == 0) {
            cv = new ContentValues();
            cv.put(Contract.DB_FIELD_ACCESS_POINT_ADDR, result.BSSID);
            cv.put(Contract.DB_FIELD_ACCESS_POINT_NAME, result.SSID);
            Uri uri = getContentResolver().insert(Contract.ACCESS_POINT_URI, cv);
            if(uri != null) {
                accessPointId = Integer.parseInt(uri.getLastPathSegment());
            } else {
                accessPointId = -1;
            }
        } else {
            cursor.moveToFirst();
            accessPointId = cursor.getInt(cursor.getColumnIndex(Contract.DB_FIELD_ID));
        }
        if(cursor != null) {
            cursor.close();
        }

        //get the RSSI if it exists, and update it, otherwise create it
        whereClause = Contract.DB_FIELD_LOCATION + "=" + mLocationId + " AND " +
                Contract.DB_FIELD_ACCESS_POINT + "=" + accessPointId + " AND " +
                Contract.DB_FIELD_DIRECTION + "=" + mCurrentCaptureDirection;
        cursor = getContentResolver().query(Contract.RSSI_URI, null, whereClause, null, null);

        int rssiId;
        if(cursor == null || cursor.getCount() == 0) {
            cv = new ContentValues();
            cv.put(Contract.DB_FIELD_NUM_SAMPLES, 1);
            cv.put(Contract.DB_FIELD_RSSI, (double)result.level);
            cv.put(Contract.DB_FIELD_LOCATION, mLocationId);
            cv.put(Contract.DB_FIELD_ACCESS_POINT, accessPointId);
            cv.put(Contract.DB_FIELD_DIRECTION, mCurrentCaptureDirection);
            Uri uri = getContentResolver().insert(Contract.RSSI_URI, cv);
            if(uri != null) {
                rssiId = Integer.parseInt(uri.getLastPathSegment());
            } else {
                rssiId = -1;
            }
        } else {
            cv = new ContentValues();
            int count;
            double rssi;
            cursor.moveToFirst();
            count = cursor.getInt(cursor.getColumnIndex(Contract.DB_FIELD_NUM_SAMPLES));
            rssi = cursor.getDouble(cursor.getColumnIndex(Contract.DB_FIELD_RSSI));
            rssiId = cursor.getInt(cursor.getColumnIndex(Contract.DB_FIELD_ID));
            rssi = ((rssi * (double)count) + (double)result.level) / (double)(count + 1);
            count++;
            cv.put(Contract.DB_FIELD_NUM_SAMPLES, count);
            cv.put(Contract.DB_FIELD_RSSI, rssi);
            getContentResolver().update(Uri.parse(Contract.RSSI_URI.toString() + "/" + rssiId), cv, null, null);
        }
        if(cursor != null) {
            cursor.close();
        }
        //add entry for raw rssi value (not useful for now, but maybe useful for later)
        cv = new ContentValues();
        cv.put(Contract.DB_FIELD_RSSI, result.level);
        cv.put(Contract.DB_FIELD_RSSI_ID, rssiId);
        getContentResolver().insert(Contract.RAW_RSSI_URI, cv);
    }

    ProgressDialog mProgress;
    private void displayPleaseWait() {
        mProgress = new ProgressDialog(this);
        mProgress.setTitle(R.string.scanning);
        setPercent(0);
        mProgress.setCancelable(true);
        mProgress.setCanceledOnTouchOutside(false);
        mProgress.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                unregisterReceiver(br);
                mProgress.dismiss();
            }
        });
        mProgress.setProgress(0);
        mProgress.show();
    }

    private void setPercent(int val) {
        mProgress.setMessage(String.format(getResources().getString(R.string.please_wait), val));
    }

    BroadcastReceiver br = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            mScansReceived++;
            captureDataPointCallback();
            setPercent(mScansReceived * 20);
            if(mScansReceived >= 5) {
                unregisterReceiver(this);
                mProgress.dismiss();
            } else {
                if(!mWifi.startScan()) {
                    unregisterReceiver(br);
                    displayError(R.string.scan_error_title, R.string.scan_error);
                }
            }
        }
    };

    private void captureDataPoint() {
        //if(mCurrentDirection != mCurrentCaptureDirection) {
        //    displayError(R.string.wrong_direction, R.string.wrong_direction_msg);
        //    return;
        //}
        mScansReceived = 0;
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION);
        registerReceiver(br, intentFilter);
        if(!mWifi.startScan()) {
            unregisterReceiver(br);
            displayError(R.string.scan_error_title, R.string.scan_error);
        } else {
            displayPleaseWait();
        }
    }

    //updates RSSI values which we have for this location, but didn't show up in this scan
    private void updateAbsentValues() {
        String whereClause = Contract.DB_FIELD_LOCATION + "=" + mLocationId + " AND " +
                Contract.DB_FIELD_DIRECTION + "=" + mCurrentCaptureDirection;
        Cursor cursor = getContentResolver().query(Contract.RSSI_URI, null, whereClause,
                null, Contract.DB_FIELD_NUM_SAMPLES + " DESC");
        if(cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            int maxCount = cursor.getInt(cursor.getColumnIndex(Contract.DB_FIELD_NUM_SAMPLES));
            for(cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                int numSamples = cursor.getInt(cursor.getColumnIndex(Contract.DB_FIELD_NUM_SAMPLES));
                if(numSamples < maxCount) {
                    double rssi = cursor.getDouble(cursor.getColumnIndex(Contract.DB_FIELD_RSSI));
                    //-100 is a minimum rssi that you're likely to see as 0 is actually a huge number
                    //it was found experimentally seeing I've never seen an RSSI less than -95
                    rssi = ((rssi * (double)numSamples) + (-100.0 * (double)(maxCount - numSamples))) / (double)maxCount;
                    ContentValues cv = new ContentValues();
                    cv.put(Contract.DB_FIELD_RSSI, rssi);
                    cv.put(Contract.DB_FIELD_NUM_SAMPLES, maxCount);
                    getContentResolver().update(Uri.parse(Contract.RSSI_URI.toString() +
                            "/" + cursor.getInt(cursor.getColumnIndex(Contract.DB_FIELD_ID))),
                            cv, null, null);
                }
            }
        }
        if(cursor != null) {
            cursor.close();
        }
    }

    private void captureDataPointCallback() {
        WifiManager wifi = (WifiManager)getSystemService(Context.WIFI_SERVICE);
        List<ScanResult> results = wifi.getScanResults();
        for(ScanResult result : results) {
            if(result.SSID.equals("CSMwireless") || result.SSID.equals("CSMguest")) {
                createOrUpdateRSSI(result);
            }
        }
        updateAbsentValues();
        if(mScansReceived >= 5) {
            switch (mCurrentCaptureDirection) {
                case Contract.NORTH:
                    mNorthDone = true;
                    break;
                case Contract.SOUTH:
                    mSouthDone = true;
                    break;
                case Contract.EAST:
                    mEastDone = true;
                    break;
                case Contract.WEST:
                    mWestDone = true;
                    break;
            }
            loadNextDirection();
        }
    }

    private void updateRequestedDirection() {
        int id;
        switch(mCurrentCaptureDirection) {
            case Contract.NORTH:
                id = R.string.north;
                break;
            case Contract.SOUTH:
                id = R.string.south;
                break;
            case Contract.EAST:
                id = R.string.east;
                break;
            case Contract.WEST:
                id = R.string.west;
                break;
            default:
                return;
        }
        ((TextView)findViewById(R.id.textFaceDirection)).setText(id);
    }
}
