package com.cs.mines.wifimodel;

import android.net.Uri;

public class Contract {
    public static final String DB_TABLE_BUILDING_INFO = "building_info";
    public static final String DB_TABLE_FLOOR_INFO = "floor_info";
    public static final String DB_TABLE_ACCESS_POINTS = "access_points";
    public static final String DB_TABLE_LOCATION_POINTS = "location_points";
    public static final String DB_TABLE_RSSIS = "rssis";
    public static final String DB_TABLE_RAW_RSSIS = "raw_rssis";
    public static final String DB_TABLE_REFERENCE_POINTS = "reference_points";
    public static final String DB_EXPORT = "export";
    public static final String DB_FIELD_ID = "_id";
    public static final String DB_FIELD_BUILDING_NAME = "_building_name";
    public static final String DB_FIELD_NUM_FLOORS = "_num_floors";
    public static final String DB_FIELD_BUILDING_ID = "_building_id";
    public static final String DB_FIELD_FLOOR_NUM = "_floor_num";
    public static final String DB_FIELD_FLOOR_ID = "_floor_id";
    public static final String DB_FIELD_PIXEL_X = "_pixel_x";
    public static final String DB_FIELD_PIXEL_Y = "_pixel_y";
    public static final String DB_FIELD_ACCESS_POINT_NAME = "_access_point_name";
    public static final String DB_FIELD_ACCESS_POINT_ADDR = "_access_point_addr";
    public static final String DB_FIELD_LATITUDE = "_latitude";
    public static final String DB_FIELD_LONGITUDE = "_longitude";
    public static final String DB_FIELD_LOCATION = "_location";
    public static final String DB_FIELD_ACCESS_POINT = "_access_point";
    public static final String DB_FIELD_RSSI = "_rssi";
    public static final String DB_FIELD_NUM_SAMPLES = "_num_samples";
    public static final String DB_FIELD_DIRECTION = "_direction";
    public static final String DB_FIELD_RSSI_ID = "_rssi_id";
    public static final String DB_FIELD_IMAGE_FILE = "_image_file";
    public static final String DB_FIELD_IMAGE_FILE_INTERNAL = "_internal_image_file";
    public static final int NORTH = 0;
    public static final int EAST = 1;
    public static final int SOUTH = 2;
    public static final int WEST = 3;
    public static final int SUCCESS = 0;
    public static final int MEDIA_NOT_MOUNTED = -1;
    public static final int CANT_COPY = -2;
    public static final int NO_SOURCE_DB = -3;
    public static final String AUTHORITY = "com.mines.cs.wifimodel.provider";
    private static final String authScope = "content://" + AUTHORITY + "/";
    //used to export database to user accessible medium
    public static final Uri EXPORT = Uri.parse(authScope + DB_EXPORT);
    public static final Uri RAW_RSSI_URI = Uri.parse(authScope + DB_TABLE_RAW_RSSIS);
    public static final Uri FLOOR_DATA_URI = Uri.parse(authScope + DB_TABLE_FLOOR_INFO);
    public static final Uri ACCESS_POINT_URI = Uri.parse(authScope + DB_TABLE_ACCESS_POINTS);
    public static final Uri BUILDING_INFO_URI = Uri.parse(authScope + DB_TABLE_BUILDING_INFO);
    public static final Uri LOCATION_URI = Uri.parse(authScope + DB_TABLE_LOCATION_POINTS);
    public static final Uri RSSI_URI = Uri.parse(authScope + DB_TABLE_RSSIS);
    public static final Uri REF_POINTS_URI = Uri.parse(authScope + DB_TABLE_REFERENCE_POINTS);
}

