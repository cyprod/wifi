package com.cs.mines.wifimodel;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.util.Log;
import android.widget.TextView;

import java.io.FileNotFoundException;
import java.text.DecimalFormat;

public class Utils {
    private static final String TAG = "Utils";
    private static final double RADIUS = 6372.8 * 1000.0; //earths radius in meters
    private static AlertDialog sAlert;
    public static class Coordinate {
        public double deltaLat;
        public double deltaLon;
    }

    public static double degreesToDistance(double lat1, double lon1, double lat2, double lon2) {
        double dLat = Math.toRadians(lat2 - lat1);
        double dLon = Math.toRadians(lon2 - lon1);
        lat1 = Math.toRadians(lat1);
        lat2 = Math.toRadians(lat2);

        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.sin(dLon / 2) * Math.sin(dLon / 2) * Math.cos(lat1) * Math.cos(lat2);
        double c = 2 * Math.asin(Math.sqrt(a));
        return RADIUS * c;
    }

    //for a given reference latitude, calculates the degree delta for a given step size for both
    //the latitude and longitude
    public static Coordinate distanceToDegrees(double referenceLat, double dist) {
        Coordinate coord = new Coordinate();
        coord.deltaLat = (dist / (Math.PI * RADIUS)) * 180.0;

        double rad = Math.cos(Math.toRadians(Math.abs(referenceLat))) * RADIUS;
        coord.deltaLon = (dist / (2.0 * Math.PI * rad)) * 360;
        return coord;
    }

    public static void displayInvalidParams(Context context) {
        AlertDialog.Builder ab = new AlertDialog.Builder(context);
        ab.setTitle(R.string.invalidParams);
        ab.setMessage(R.string.paramsNotValid);
        ab.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                sAlert.dismiss();
            }
        });
        ab.setCancelable(false);
        sAlert = ab.show();
    }

    public static int setDirectionText(double direction, TextView view, Context context) {
        int dir, newDirection;
        if(direction >= -45 && direction < 45) {
            dir = R.string.north;
            newDirection = Contract.NORTH;
        } else if(direction >= 45 && direction < 135) {
            dir = R.string.east;
            newDirection = Contract.EAST;
        } else if (direction <= -45 && direction > -135) {
            dir = R.string.west;
            newDirection = Contract.WEST;
        } else {
            dir = R.string.south;
            newDirection = Contract.SOUTH;
        }
        DecimalFormat format = new DecimalFormat("0.000000");
        String text = format.format(direction) + " (" + context.getResources().getString(dir) + ")";
        view.setText(text);
        return newDirection;
    }

    public static Bitmap getBitmap(Context context, String imageName) {
        ParcelFileDescriptor fd;
        Log.d(TAG, "Getting file: " + imageName);
        try {
            fd = context.getContentResolver().openFileDescriptor(
                    Uri.parse(Contract.FLOOR_DATA_URI.toString() + "/" + imageName), "r");
        } catch (FileNotFoundException e) {
            //display error dialog
            e.printStackTrace();
            return null;
        }
        return BitmapFactory.decodeFileDescriptor(fd.getFileDescriptor());
    }
}
