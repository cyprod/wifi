package com.cs.mines.wifimodel;

public interface GpsViewCallback {
    public void ReceiveCoordinateData(float bitmapX, float bitmapY);
}
