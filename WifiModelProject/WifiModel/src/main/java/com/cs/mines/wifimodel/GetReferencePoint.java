package com.cs.mines.wifimodel;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Locale;

public class GetReferencePoint extends Activity implements GpsViewCallback{
    private static final String TAG = "GetReferencePoint";
    public static final String BUILDING_ID = "building_id";
    public static final String FLOOR_ID = "floor_id";
    public static final String FLOOR_NUMBER = "floor_number";
    public static final String IMAGE_X_OFFSET = "x_offset";
    public static final String IMAGE_Y_OFFSET = "y_offset";
    public static final String REF_POINT_ID = "ref_id";
    public static final String LATITUDE = "latitude";
    public static final String LONGITUDE = "longitude";

    public static final int TRAINING_DATA = 1;
    public static final int RESULT_FAILURE = 0;
    public static final int RESULT_INSERT = 1;
    public static final int RESULT_UPDATE = 2;

    private int mBuildingId;
    private int mFloorId;
    private int mFloorNumber;
    private int mRefPointId;
    private boolean mPointExists;
    private GpsImageView mView;
    private View mDialogView;
    private AlertDialog mDialog;
    private float mX, mY;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "entered onCreate");
        setContentView(R.layout.get_data_point);
        Intent launchingIntent = getIntent();
        mBuildingId = launchingIntent.getIntExtra(BUILDING_ID, -1);
        mFloorId = launchingIntent.getIntExtra(FLOOR_ID, -1);
        mFloorNumber = launchingIntent.getIntExtra(FLOOR_NUMBER, -1);
        if(mBuildingId < 0 || mFloorId < 0 || mFloorNumber < 0) {
            throw new IllegalArgumentException("Error launched with invalid building id "
                    + mBuildingId + "or floor id " + mFloorId + " or floor number " + mFloorNumber);
        }
        mView = (GpsImageView)findViewById(R.id.zoomImage);
        mView.registerCallback(this);
        Bitmap bitmap = getBitmap();
        if(bitmap != null) {
            mView.setImage(bitmap);
        }
        ((TextView)findViewById(R.id.textZoom)).setText(String.format(Locale.getDefault(), getResources().getString(R.string.floor), mFloorNumber + 1));
    }

    @Override
    protected void onPause() {
        super.onPause(); Log.d(TAG, "onPause"); finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy");
    }

    @Override
    public void ReceiveCoordinateData(float bitmapX, float bitmapY) {
        mPointExists = false;
        float lat = 0, lon = 0;
        mX = bitmapX;
        mY = bitmapY;
        String whereClause = Contract.DB_FIELD_FLOOR_ID + "=" + mFloorId + " AND " +
                Contract.DB_FIELD_FLOOR_ID + "=" + mFloorId + " AND " +
                Contract.DB_FIELD_PIXEL_X + "=" + bitmapX + " AND " +
                Contract.DB_FIELD_PIXEL_Y + "=" + bitmapY;
        Cursor cursor = getContentResolver().query(Contract.REF_POINTS_URI, null, whereClause, null, null);
        if(cursor != null && cursor.getCount() != 0) {
            cursor.moveToFirst();
            mPointExists = true;
            lat = cursor.getFloat(cursor.getColumnIndex(Contract.DB_FIELD_LATITUDE));
            lon = cursor.getFloat(cursor.getColumnIndex(Contract.DB_FIELD_LONGITUDE));
            mRefPointId = cursor.getInt(cursor.getColumnIndex(Contract.DB_FIELD_ID));
        }
        if(cursor != null) {
            cursor.close();
        }
        mDialogView = View.inflate(this, R.layout.capture_dialog, null);
        ((TextView)mDialogView.findViewById(R.id.textDialogTitle)).setText(
                String.format(getResources().getString(R.string.coordinate), bitmapX, bitmapY));
        if(mPointExists) {
            ((EditText)findViewById(R.id.editLon)).setText(Float.toString(lon));
            ((EditText)findViewById(R.id.editLat)).setText(Float.toString(lat));
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.dialog_title);
        builder.setView(mDialogView);
        builder.setCancelable(false);
        builder.setPositiveButton(getResources().getString(android.R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                String lonText = ((EditText)mDialogView.findViewById(R.id.editLon)).getText().toString();
                String latText = ((EditText)mDialogView.findViewById(R.id.editLat)).getText().toString();
                if(lonText.length() > 0 && latText.length() > 0) {
                    float lat, lon;
                    try {
                        lat = Float.parseFloat(latText);
                        lon = Float.parseFloat(lonText);
                    } catch (NumberFormatException e) {
                        Utils.displayInvalidParams(GetReferencePoint.this);
                        return;
                    }
                    Intent retIntent = new Intent();
                    retIntent.putExtra(IMAGE_X_OFFSET, mX);
                    retIntent.putExtra(IMAGE_Y_OFFSET, mY);
                    retIntent.putExtra(LATITUDE, lat);
                    retIntent.putExtra(LONGITUDE, lon);
                    if(mPointExists) {
                        retIntent.putExtra(REF_POINT_ID, mRefPointId);
                        setResult(RESULT_INSERT, retIntent);
                    } else {
                        setResult(RESULT_INSERT, retIntent);
                    }
                    mDialog.dismiss();
                    finish();
                } else {
                    Utils.displayInvalidParams(GetReferencePoint.this);
                }
            }
        });
        builder.setNegativeButton(getResources().getString(android.R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                mDialog.dismiss();
                Intent retIntent = new Intent();
                setResult(RESULT_FAILURE, retIntent);
                finish();
            }
        });
        mDialog = builder.create();
        mDialog.show();
    }

    private Bitmap getBitmap() {
        String imageName = "";
        Cursor cursor = getContentResolver().query(Uri.parse(Contract.FLOOR_DATA_URI.toString() + "/" + mFloorId),
                null, null, null, null);
        if(cursor != null && cursor.getCount() == 1) {
            cursor.moveToFirst();
            imageName = cursor.getString(cursor.getColumnIndex(Contract.DB_FIELD_IMAGE_FILE_INTERNAL));
        }
        if(cursor != null) {
            cursor.close();
        }

        if(imageName == null || imageName.equals("")) {
            Log.d(TAG, "Error imageName invalid: '" + imageName + "'");
            finish();
            return null;
        }
        Bitmap retVal = Utils.getBitmap(this, imageName);

        if(retVal == null) {
            Log.d(TAG, "Error getting bitmap");
            finish();
        }
        return retVal;
    }
}
