package com.cs.mines.wifimodel;

public interface CompassCallback {
    public void getDirection(double direction);
}
