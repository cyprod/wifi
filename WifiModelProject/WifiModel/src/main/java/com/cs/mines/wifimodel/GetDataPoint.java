package com.cs.mines.wifimodel;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class GetDataPoint extends Activity implements GpsViewCallback{
    public static final String BUILDING_ID = "building_id";

    private int mBuildingId;
    private int mCurrentFloor;
    private int mMaxFloor;
    private FloorInfo[] mFloors;
    private GpsImageView mView;
    private TextView mTextView;
    private double mZeroLat;
    private double mZeroLon;
    private double mZeroX;
    private double mZeroY;
    private double mLatPerPixel = 0f;
    private double mLonPerPixel = 0f;

    @Override
    public void ReceiveCoordinateData(float bitmapX, float bitmapY) {
        int id;
        Log.d("BLAH", "bitmapX " + bitmapX + " bitmapY " + bitmapY + " lonPerPixel " + mLonPerPixel + " latPerPixel " + mLatPerPixel + " zeroLon " + mZeroLon + " zeroLat " + mZeroLat);
        double currentLon = ((bitmapX - mZeroX) * mLonPerPixel) + mZeroLon;
        //opposite since (0,0) is the upper left corner, not lower left corner
        double currentLat = ((mZeroY - bitmapY) * mLatPerPixel) + mZeroLat;
        Log.d("BLAHBLAH", "currentLon: " + currentLon + ", currentLat: " + currentLat);
        String whereClause = Contract.DB_FIELD_FLOOR_ID + "=" + mFloors[mCurrentFloor].floorId + " AND " +
                Contract.DB_FIELD_PIXEL_X + "=" + bitmapX + " AND " +
                Contract.DB_FIELD_PIXEL_Y + "=" + bitmapY;
        String[] selection = new String[1];
        selection[0] = Contract.DB_FIELD_ID;
        Cursor cursor = getContentResolver().query(Contract.LOCATION_URI, selection, whereClause, null, null);
        if(cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            id = cursor.getInt(cursor.getColumnIndex(Contract.DB_FIELD_ID));
            if(cursor != null) {
                cursor.close();
            }
        } else {
            ContentValues cv = new ContentValues();
            cv.put(Contract.DB_FIELD_LATITUDE, currentLat);
            cv.put(Contract.DB_FIELD_LONGITUDE, currentLon);
            cv.put(Contract.DB_FIELD_PIXEL_X, bitmapX);
            cv.put(Contract.DB_FIELD_PIXEL_Y, bitmapY);
            cv.put(Contract.DB_FIELD_FLOOR_ID, mFloors[mCurrentFloor].floorId);
            id = Integer.parseInt(getContentResolver().insert(Contract.LOCATION_URI, cv).getLastPathSegment());
        }

        //launch CaptureScreen.java
        Intent intent = new Intent(this, CaptureScreen.class);
        intent.putExtra(CaptureScreen.LOCATION_ID, id);
        startActivity(intent);
    }

    private enum FloorDirection {
        UP,
        DOWN
    }

    private class FloorInfo {
        public int floorId;
        public int floorNum;
        public String imageName;
    }

    private class RefPoint {
        public float x;
        public float y;
        public float lat;
        public float lon;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.get_data_point);
        Intent launchingIntent = getIntent();
        mBuildingId = launchingIntent.getIntExtra(BUILDING_ID, -1);
        if(mBuildingId < 0) {
            throw new IllegalArgumentException("Error: invalid building id");
        }

        mView = (GpsImageView) findViewById(R.id.zoomImage);
        mTextView = (TextView)findViewById(R.id.textZoom);
        mCurrentFloor = 0;

        mView.registerCallback(this);
        getBuildingInfo();
        loadFloor();

        (findViewById(R.id.upDownLayout)).setVisibility(View.VISIBLE);
        (findViewById(R.id.butUp)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setFloor(FloorDirection.UP);
            }
        });
        (findViewById(R.id.butDown)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setFloor(FloorDirection.DOWN);
            }
        });
        (findViewById(R.id.butExport)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                exportFile();
            }
        });
    }

    private void exportFile() {
        String toastText;
        String fileName = "exported.db";
        //create dummy cv so android doesn't crash
        ContentValues cv = new ContentValues();
        int retVal = getContentResolver().update(Uri.parse(Contract.EXPORT.toString() +
                "/" + fileName), cv, null, null);
        switch(retVal) {
            case Contract.CANT_COPY:
                toastText = getResources().getString(R.string.cant_copy);
                break;
            case Contract.MEDIA_NOT_MOUNTED:
                toastText = getResources().getString(R.string.not_mounted);
                break;
            case Contract.NO_SOURCE_DB:
                toastText = getResources().getString(R.string.no_source_db);
                break;
            case Contract.SUCCESS:
                toastText = String.format(getResources().getString(R.string.database_exported), fileName);
                break;
            default:
                toastText = getResources().getString(R.string.error_exporting);
                break;
        }
        Toast.makeText(this, toastText, Toast.LENGTH_LONG).show();
    }

    private void setFloor(FloorDirection direction) {
        boolean reload = true;
        switch(direction) {
            case UP:
                mCurrentFloor++;
                break;
            case DOWN:
                mCurrentFloor--;
                break;
        }
        if(mCurrentFloor >= mMaxFloor) {
            mCurrentFloor = mMaxFloor - 1;
            reload = false;
        }
        if(mCurrentFloor < 0) {
            mCurrentFloor = 0;
            reload = false;
        }
        if(reload) {
            loadFloor();
        }
    }

    private void getBuildingInfo() {
        String whereClause = Contract.DB_FIELD_BUILDING_ID + "=" + mBuildingId;
        Cursor cursor = getContentResolver().query(Contract.FLOOR_DATA_URI, null, whereClause, null, Contract.DB_FIELD_FLOOR_NUM + " ASC");

        if(cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            mMaxFloor = cursor.getCount();
            mFloors = new FloorInfo[cursor.getCount()];
            int i = 0;
            for(cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                mFloors[i] = new FloorInfo();
                mFloors[i].floorId = cursor.getInt(cursor.getColumnIndex(Contract.DB_FIELD_ID));
                mFloors[i].floorNum = cursor.getInt(cursor.getColumnIndex(Contract.DB_FIELD_FLOOR_NUM));
                mFloors[i].imageName = cursor.getString(cursor.getColumnIndex(Contract.DB_FIELD_IMAGE_FILE_INTERNAL));
                i++;
            }
        }

        if(cursor != null) {
            cursor.close();
        }
    }

    private void loadFloor() {
        getTrainingInfo();
        mTextView.setText(String.format(getResources().getString(R.string.floor), mFloors[mCurrentFloor].floorNum + 1));
        Bitmap bitmap = Utils.getBitmap(this, mFloors[mCurrentFloor].imageName);
        if(bitmap != null) {
            mView.setImage(bitmap);
        }
        mView.resetZoom();
    }

    private void getTrainingInfo() {
        RefPoint[] refPoints;
        String whereClause = Contract.DB_FIELD_FLOOR_ID + "=" + mFloors[mCurrentFloor].floorId;
        Cursor cursor = getContentResolver().query(Contract.REF_POINTS_URI, null, whereClause, null, null);

        if(cursor != null && cursor.getCount() > 1) {
            refPoints = new RefPoint[cursor.getCount()];
            cursor.moveToFirst();
            for(int i = 0; i < cursor.getCount(); i++) {
                refPoints[i] = new RefPoint();
                refPoints[i].x = cursor.getFloat(cursor.getColumnIndex(Contract.DB_FIELD_PIXEL_X));
                refPoints[i].y = cursor.getFloat(cursor.getColumnIndex(Contract.DB_FIELD_PIXEL_Y));
                refPoints[i].lat = cursor.getFloat(cursor.getColumnIndex(Contract.DB_FIELD_LATITUDE));
                refPoints[i].lon = cursor.getFloat(cursor.getColumnIndex(Contract.DB_FIELD_LONGITUDE));
                cursor.moveToNext();
            }
        } else {
            throw new IllegalArgumentException("Error: need two or more reference points");
        }

        if(cursor != null) {
            cursor.close();
        }

        //average the distance between all reference points to calculate how the lat/lon distance
        //between one pixel
        mLatPerPixel = mLonPerPixel = 0.0f;
        int xNumPoints = 0, yNumPoints = 0;
        double xDiff = 0, yDiff = 0, latDiff = 0, lonDiff = 0;
        for(int i = 0; i < refPoints.length; i++) {
            for(int j = i + 1; j < refPoints.length; j++) {
                xDiff += Math.abs(refPoints[i].x - refPoints[j].x);
                yDiff += Math.abs(refPoints[i].y - refPoints[j].y);
                latDiff += Math.abs(refPoints[i].lat - refPoints[j].lat);
                lonDiff += Math.abs(refPoints[i].lon - refPoints[j].lon);
                yNumPoints++;
                xNumPoints++;
                mLatPerPixel = latDiff / yDiff;
                mLonPerPixel = lonDiff / xDiff;
                Log.d("TAG", "ynumpoints " + yNumPoints +
                        "xnumpoints " + xNumPoints +
                        " xi " + refPoints[i].x +
                        " yi " + refPoints[i].y +
                        " xj " + refPoints[j].x +
                        " yj " + refPoints[j].y +
                        " lati " + refPoints[i].lat +
                        " loni " + refPoints[i].lon +
                        " latj " + refPoints[j].lat +
                        " lonj " + refPoints[j].lon +
                        " xdiff " + xDiff +
                        " ydiff " + yDiff +
                        " latdiff " + latDiff +
                        " londiff " + lonDiff +
                        " mLatPerPixel " + mLatPerPixel +
                        " mLonPerPixel " + mLonPerPixel );
            }
        }

        //calculate the lat/lon of (0,0) of the image for future reference
        double x , y, lat, lon;
        x = y = lat = lon = 0;
        for(RefPoint point : refPoints) {
            x += point.x;
            y += point.y;
            lat += point.lat;
            lon += point.lon;
        }
        x /= refPoints.length;
        y /= refPoints.length;
        lat /= refPoints.length;
        lon /= refPoints.length;
        Log.d("BLAH2", "x " + x + " y " + y + " lat " + lat + " lon " + lon);

        mZeroX = x;
        mZeroY = y;
        mZeroLat = lat;
        mZeroLon = lon;
        Log.d("BLAH3", "mzerolat " + mZeroLat + " mzerolon " + mZeroLon);
    }
}
