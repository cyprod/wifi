package com.cs.mines.wifimodel;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;

public class GpsImageView extends View {

    private boolean dragged;
    private Bitmap mBitmap = null;
    private GpsViewCallback mCallback;

    //These two constants specify the minimum and maximum zoom
    private static float MIN_ZOOM = 1f;
    private static float MAX_ZOOM = 5f;

    private float scaleFactor = 1.f;
    private ScaleGestureDetector detector;

    //These constants specify the mode that we're in
    private static int NONE = 0;
    private static int DRAG = 1;
    private static int ZOOM = 2;

    private int mode;

    //These two variables keep track of the X and Y coordinate of the finger when it first
    //touches the screen
    private float startX = 0f;
    private float startY = 0f;

    //These two variables keep track of the amount we need to translate the canvas along the X
    //and the Y coordinate
    private float translateX = 0f;
    private float translateY = 0f;

    //These two variables keep track of the amount we translated the X and Y coordinates, the last time we
    //panned.
    private float previousTranslateX = 0f;
    private float previousTranslateY = 0f;

    private void getMinScale() {
        float xZoom = this.getWidth() / (float)mBitmap.getWidth();
        float yZoom = this.getHeight() / (float)mBitmap.getHeight();
        MIN_ZOOM =  xZoom < yZoom ? xZoom : yZoom;
        Log.d("blah", "xZoom: " + xZoom + " yZoom: " + yZoom);
        scaleFactor = MIN_ZOOM;
    }

    public GpsImageView(Context context) {
        super(context);
        detector = new ScaleGestureDetector(getContext(), new ScaleListener());
        //mBitmap = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.enhanceimageframe);
    }

    public GpsImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        detector = new ScaleGestureDetector(getContext(), new ScaleListener());
        //mBitmap = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.enhanceimageframe);
    }

    public GpsImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        detector = new ScaleGestureDetector(getContext(), new ScaleListener());
        //mBitmap = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.enhanceimageframe);
    }

    public void setImage(Bitmap bitmap) {
        mBitmap = bitmap;
    }

    public void registerCallback(GpsViewCallback callback) {
        mCallback = callback;
    }

    public void resetZoom() {
        scaleFactor = MIN_ZOOM;
        invalidate();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        switch (event.getAction() & MotionEvent.ACTION_MASK) {

            case MotionEvent.ACTION_DOWN:
                mode = DRAG;

                //We assign the current X and Y coordinate of the finger to startX and startY minus the previously translated
                //amount for each coordinates This works even when we are translating the first time because the initial
                //values for these two variables is zero.
                startX = event.getX() - previousTranslateX;
                startY = event.getY() - previousTranslateY;
                break;

            case MotionEvent.ACTION_MOVE:
                translateX = event.getX() - startX;
                translateY = event.getY() - startY;

                //We cannot use startX and startY directly because we have adjusted their values using the previous translation values.
                //This is why we need to add those values to startX and startY so that we can get the actual coordinates of the finger.
                double distance = Math.sqrt(Math.pow(event.getX() - (startX + previousTranslateX), 2) +
                                Math.pow(event.getY() - (startY + previousTranslateY), 2)
                );

                if (distance > 5) {
                    dragged = true;
                }

                break;

            case MotionEvent.ACTION_POINTER_DOWN:
                mode = ZOOM;
                break;

            case MotionEvent.ACTION_UP:
                mode = NONE;
                if(!dragged) {
                    calculateGps(startX, startY);
                }
                dragged = false;

                //All fingers went up, so let's save the value of translateX and translateY into previousTranslateX and
                //previousTranslate
                previousTranslateX = translateX;
                previousTranslateY = translateY;
                break;

            case MotionEvent.ACTION_POINTER_UP:
                mode = DRAG;

                //This is not strictly necessary; we save the value of translateX and translateY into previousTranslateX
                //and previousTranslateY when the second finger goes up
                previousTranslateX = translateX;
                previousTranslateY = translateY;
                break;
        }

        detector.onTouchEvent(event);

        //We redraw the canvas only in the following cases:
        //
        // o The mode is ZOOM
        //        OR
        // o The mode is DRAG and the scale factor is not equal to 1 (meaning we have zoomed) and dragged is
        //   set to true (meaning the finger has actually moved)
        if ((mode == DRAG && scaleFactor != 1f && dragged) || mode == ZOOM) {
            invalidate();
        }

        return true;
    }

    private void calculateGps(float x, float y) {
        if(mCallback != null && mBitmap != null) {
            float realX = x / scaleFactor;
            float realY = y / scaleFactor;
            if(realX < 0 || realX > mBitmap.getWidth() || realY < 0 || realY > mBitmap.getHeight()) {
                return;
            }
            mCallback.ReceiveCoordinateData(realX, realY);
        }
    }

    private boolean isFirst = true;
    @Override
    public void onDraw(Canvas canvas) {
        if(mBitmap == null) {
            super.onDraw(canvas);
            return;
        }
        if(isFirst) {
            isFirst = false;
            getMinScale();
        }
        Matrix matrix = new Matrix();
        matrix.reset();

        //We're going to scale the X and Y coordinates by the same amount
        matrix.postScale(scaleFactor, scaleFactor);

        //This is where we take care of the right bound. We compare translateX times -1 to (scaleFactor - 1) * displayWidth.
        //If translateX is greater than that value, then we know that we've gone over the bound. So we set the value of
        //translateX to (1 - scaleFactor) times the display width. Notice that the terms are interchanged; it's the same
        //as doing -1 * (scaleFactor - 1) * displayWidth
        if ((translateX * -1f) > (((float)mBitmap.getWidth()) * scaleFactor) - this.getWidth()) {
            translateX = -1f * ((((float)mBitmap.getWidth()) * scaleFactor) - ((float)this.getWidth()));
            previousTranslateX = translateX;
        }
        //else if ((translateX * -1f) > (scaleFactor - MIN_ZOOM) * (mBitmap.getWidth() /*/ MIN_ZOOM*/)) {
        //    translateX = (MIN_ZOOM - scaleFactor) * (mBitmap.getWidth() /*/ MIN_ZOOM*/);
        //    previousTranslateX = translateX;
        //}

        //If translateX times -1 is lesser than zero, let's set it to zero. This takes care of the left bound
        if ((translateX * -1) < 0) {
            translateX = 0;
            previousTranslateX = 0;
        }

        //We do the exact same thing for the bottom bound, except in this case we use the height of the display
        if ((translateY * -1f) > (mBitmap.getHeight() * scaleFactor) - this.getHeight()) {
            translateY = -1f * ((mBitmap.getHeight() * scaleFactor) - this.getHeight());
            previousTranslateY = translateY;
        }
        //else if ((translateY * -1f) > (scaleFactor - MIN_ZOOM) * (mBitmap.getHeight() /*/ MIN_ZOOM*/)) {
        //    translateY = (MIN_ZOOM - scaleFactor) * (mBitmap.getHeight() /*/ MIN_ZOOM*/);
        //    previousTranslateY = translateY;
        //}

        if (translateY * -1 < 0) {
            translateY = 0;
            previousTranslateY = 0;
        }

        //We need to divide by the scale factor here, otherwise we end up with excessive panning based on our zoom level
        //because the translation amount also gets scaled according to how much we've zoomed into the canvas.
        matrix.postTranslate(translateX /*/ scaleFactor*/, translateY /*/ scaleFactor*/);

        canvas.drawBitmap(mBitmap, matrix, null);
        /* The rest of your canvas-drawing code */
        canvas.restore();
    }

    private class ScaleListener extends ScaleGestureDetector.SimpleOnScaleGestureListener {
        @Override
        public boolean onScale(ScaleGestureDetector detector) {
            scaleFactor *= detector.getScaleFactor();
            scaleFactor = Math.max(MIN_ZOOM, Math.min(scaleFactor, MAX_ZOOM));
            return true;
        }
    }
}
