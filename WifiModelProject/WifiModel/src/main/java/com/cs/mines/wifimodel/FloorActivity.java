package com.cs.mines.wifimodel;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class FloorActivity extends Activity {
    public static final String NUM_FLOORS = "num_floors";
    public static final String POINT_DISTANCE = "point_distance";
    public static final String BUILDING_ID = "building_id";
    private ListView mDataPoints;
    private boolean mNextScreenLaunched;
    private int mNumFloors;
    private int mBuildingId;
    private int mCurrentFloor;
    private int mFloorId;
    private boolean mFloorExists;
    private ArrayAdapter<String> mAdapter;
    private Map<String, Integer> mIds;
    private AlertDialog mAlert;
    private String mSelectedItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.get_floor_info);
        mNextScreenLaunched = false;
        Intent launchingIntent = getIntent();
        mNumFloors = launchingIntent.getIntExtra(NUM_FLOORS, 0);
        mBuildingId = launchingIntent.getIntExtra(BUILDING_ID, -1);
        if(mNumFloors <= 0 || mBuildingId < 0) {
            throw new IllegalArgumentException("Launching intent included " + mNumFloors +
                    " floor(s) (minimum 1) and building ID of: " + mBuildingId);
        }
        mCurrentFloor = 0;
        mIds = new HashMap<String, Integer>();

        mDataPoints = (ListView)findViewById(R.id.dataPoints);
        updateFloorNum();

        (findViewById(R.id.butAdd)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                captureDataPoint();
            }
        });
        (findViewById(R.id.butDel)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteDataPoint();
            }
        });
        (findViewById(R.id.butPrev)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCurrentFloor--;
                if(mCurrentFloor < 0) {
                    mCurrentFloor = 0;
                }
                updateFloorNum();
                loadFloorIfExists();
            }
        });
        (findViewById(R.id.butNext)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCurrentFloor++;
                if(mCurrentFloor >= mNumFloors) {
                    mCurrentFloor = mNumFloors - 1;
                }
                updateFloorNum();
                loadFloorIfExists();
            }
        });
        (findViewById(R.id.butDone)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                launchNextScreen();
            }
        });

        mDataPoints.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                for(int j = 0; j < adapterView.getChildCount(); j++) {
                    adapterView.getChildAt(j).setBackgroundColor(Color.TRANSPARENT);
                }
                view.setBackgroundColor(Color.LTGRAY);
                mSelectedItem = (String)adapterView.getItemAtPosition(i);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        loadFloorIfExists();
        if(mNextScreenLaunched) {
            finish();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == GetReferencePoint.TRAINING_DATA &&
                (resultCode == GetReferencePoint.RESULT_INSERT || resultCode == GetReferencePoint.RESULT_UPDATE)) {
            float xVal, yVal, lat, lon;
            xVal = data.getFloatExtra(GetReferencePoint.IMAGE_X_OFFSET, Float.MIN_VALUE);
            yVal = data.getFloatExtra(GetReferencePoint.IMAGE_Y_OFFSET, Float.MIN_VALUE);
            lat = data.getFloatExtra(GetReferencePoint.LATITUDE, Float.MIN_VALUE);
            lon = data.getFloatExtra(GetReferencePoint.LONGITUDE, Float.MIN_VALUE);
            if(xVal == Float.MIN_VALUE || yVal == Float.MIN_VALUE || lat == Float.MIN_VALUE || lon == Float.MIN_VALUE) {
                throw new IllegalArgumentException("Error: result contains incomplete data");
            }
            ContentValues cv = new ContentValues();
            cv.put(Contract.DB_FIELD_LONGITUDE, lon);
            cv.put(Contract.DB_FIELD_LATITUDE, lat);
            if(resultCode == GetReferencePoint.RESULT_INSERT) {
                cv.put(Contract.DB_FIELD_FLOOR_ID, mFloorId);
                cv.put(Contract.DB_FIELD_PIXEL_X, xVal);
                cv.put(Contract.DB_FIELD_PIXEL_Y, yVal);
                getContentResolver().insert(Contract.REF_POINTS_URI, cv);
            } else {
                int refId = data.getIntExtra(GetReferencePoint.REF_POINT_ID, -1);
                if(refId < 0) {
                    throw new IllegalArgumentException("Error: result contains incomplete data");
                } else {
                    getContentResolver().update(Uri.parse(Contract.REF_POINTS_URI.toString() + "/" + refId), cv, null, null);
                }
            }
        }
    }

    private boolean validate() {
        boolean retVal = true;
        String whereClause = Contract.DB_FIELD_BUILDING_ID + "=" + mBuildingId;
        String[] projection = new String[1];
        projection[0] = Contract.DB_FIELD_ID;
        Cursor cursor = getContentResolver().query(Contract.FLOOR_DATA_URI, projection, whereClause, null, null);
        if(cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            while(!cursor.isAfterLast() && retVal) {
                int floorId = cursor.getInt(cursor.getColumnIndex(Contract.DB_FIELD_ID));
                whereClause = Contract.DB_FIELD_FLOOR_ID + "=" + floorId;
                Cursor cursor2 = getContentResolver().query(Contract.REF_POINTS_URI, projection, whereClause, null, null);
                if(cursor2 == null || cursor2.getCount() < 2) {
                    retVal = false;
                }
                if(cursor2 != null) {
                    cursor2.close();
                }
                cursor.moveToNext();
            }
        } else {
            throw new IllegalArgumentException("Error, 0 floor ids returned");
        }

        if(cursor != null) {
            cursor.close();
        }
        return retVal;
    }

    private void launchNextScreen() {
        if(!validate()) {
            Toast.makeText(this, getResources().getString(R.string.more_ref_points), Toast.LENGTH_LONG).show();
            return;
        }
        Intent intent = new Intent(this, GetDataPoint.class);
        intent.putExtra(GetDataPoint.BUILDING_ID, mBuildingId);
        startActivity(intent);
    }

    private void captureDataPoint() {
        Intent getPoint = new Intent(this, GetReferencePoint.class);
        getPoint.putExtra(GetReferencePoint.BUILDING_ID, mBuildingId);
        getPoint.putExtra(GetReferencePoint.FLOOR_ID, mFloorId);
        getPoint.putExtra(GetReferencePoint.FLOOR_NUMBER, mCurrentFloor);
        startActivityForResult(getPoint, GetReferencePoint.TRAINING_DATA);
    }

    private void deleteDataPoint() {
        final Integer id;
        if(mSelectedItem == null) {
            return;
        }
        id = mIds.get(mSelectedItem);
        if(id == null) {
            Toast.makeText(this, R.string.error_getting_item, Toast.LENGTH_LONG).show();
            return;
        }
        AlertDialog.Builder ab = new AlertDialog.Builder(this);
        ab.setTitle(R.string.delete);
        ab.setMessage(R.string.are_you_sure);
        ab.setCancelable(false);
        ab.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                mAlert.dismiss();
                getContentResolver().delete(Uri.parse(Contract.REF_POINTS_URI.toString() + "/" + id), null, null);
                loadFloorIfExists();
            }
        });
        ab.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                mAlert.dismiss();
            }
        });
        mAlert = ab.show();
    }

    private void loadFloorIfExists() {
        ArrayList<String> values = new ArrayList<String>();
        mIds.clear();
        String whereClause = Contract.DB_FIELD_BUILDING_ID + "=" + mBuildingId + " AND " +
                Contract.DB_FIELD_FLOOR_NUM + "=" + mCurrentFloor;
        //extract floorId
        Cursor cursor = getContentResolver().query(Contract.FLOOR_DATA_URI, null, whereClause, null, null);
        if(cursor != null && cursor.getCount() == 1) {
            cursor.moveToFirst();
            mFloorId = cursor.getInt(cursor.getColumnIndex(Contract.DB_FIELD_ID));
            cursor.close();
        } else {
            if(cursor != null) {
                cursor.close();
            }
            throw new IllegalArgumentException("Error: Table ID not found");
        }
        //create a new query with floorId
        whereClause = Contract.DB_FIELD_FLOOR_ID + "=" + mFloorId;
        cursor = getContentResolver().query(Contract.REF_POINTS_URI, null, whereClause, null, null);
        if(cursor != null && cursor.getCount() > 0) {
            for(cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                String value = "(";
                value += cursor.getFloat(cursor.getColumnIndex(Contract.DB_FIELD_PIXEL_X)) + ",";
                value += cursor.getFloat(cursor.getColumnIndex(Contract.DB_FIELD_PIXEL_Y)) + "), (";
                value += cursor.getFloat(cursor.getColumnIndex(Contract.DB_FIELD_LONGITUDE)) + ",";
                value += cursor.getFloat(cursor.getColumnIndex(Contract.DB_FIELD_LATITUDE)) + ")";
                mIds.put(value, cursor.getInt(cursor.getColumnIndex(Contract.DB_FIELD_ID)));
                values.add(value);
            }
        }
        if(cursor != null) {
            cursor.close();
        }

        mAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, values);
        mDataPoints.setAdapter(mAdapter);
        mSelectedItem = null;
    }

    private void updateFloorNum() {
        ((TextView)findViewById(R.id.txtFloorNum)).setText(Integer.toString(mCurrentFloor + 1));
    }
}
