package com.cs.mines.wifimodel;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;

public class FloorResourcesActivity extends Activity {
    public static final String NUM_FLOORS = "num_floors";
    public static final String BUILDING_ID = "building_id";

    private class EditContainer {
        EditText editText;
        long id = -1;
    }

    private static int mNumFloors;
    private static int mBuildingId;
    private static LinearLayout mLayout;
    private EditContainer[] mResNames;
    private Button mButton;
    private boolean mNextScreenLaunched = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.floor_resources);
        Intent launchingIntent = getIntent();
        mNumFloors = launchingIntent.getIntExtra(NUM_FLOORS, 0);
        mBuildingId = launchingIntent.getIntExtra(BUILDING_ID, -1);
        if(mNumFloors <= 0 || mBuildingId < 0) {
            throw new IllegalArgumentException("Launching intent included " + mNumFloors +
                    " floor(s) (minimum 1) and building ID of: " + mBuildingId);
        }
        mLayout = (LinearLayout)findViewById(R.id.ResourceLayout);
        mButton = (Button)findViewById(R.id.resNext);
        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                verifyAndLoad();
            }
        });
        mResNames = new EditContainer[mNumFloors];
        for(int i = 0; i < mNumFloors; i++) {
            mResNames[i] = new EditContainer();
            mResNames[i].editText = addEditText(i);
        }
        prepopulate();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(mNextScreenLaunched) {
            finish();
        }
    }

    private void verifyAndLoad() {
        boolean allPopulated = true;

        for(EditContainer edit : mResNames) {
            String text = edit.editText.getText().toString();
            if(text.length() == 0) {
                allPopulated = false;
                break;
            }
        }
        if(!allPopulated) {
            Utils.displayInvalidParams(this);
            return;
        }

        File baseDirectory = Environment.getExternalStorageDirectory();
        for(int i = 0; i < mResNames.length; i++) {
            String path = baseDirectory + "/" + mResNames[i].editText.getText();
            if(mResNames[i].id > 0) {
                ContentValues cv = new ContentValues();
                cv.put(Contract.DB_FIELD_IMAGE_FILE, path);
                try {
                    getContentResolver().update(Uri.parse(Contract.FLOOR_DATA_URI.toString() + "/" + mResNames[i].id), cv, null, null);
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                    Toast.makeText(FloorResourcesActivity.this, getResources().getString(R.string.file_not_found), Toast.LENGTH_LONG).show();
                }
            } else {
                ContentValues cv = new ContentValues();
                cv.put(Contract.DB_FIELD_IMAGE_FILE, path);
                cv.put(Contract.DB_FIELD_BUILDING_ID, mBuildingId);
                cv.put(Contract.DB_FIELD_FLOOR_NUM, i);
                getContentResolver().insert(Contract.FLOOR_DATA_URI, cv);
            }
        }
        mNextScreenLaunched = true;
        Intent nextScreen = new Intent(this, FloorActivity.class);
        nextScreen.putExtra(FloorActivity.NUM_FLOORS, mNumFloors);
        nextScreen.putExtra(FloorActivity.BUILDING_ID, mBuildingId);
        startActivity(nextScreen);
    }

    private void prepopulate() {
        for(int i = 0; i < mResNames.length; i++) {
            String whereClause = Contract.DB_FIELD_BUILDING_ID + "=" + mBuildingId + " AND " +
                    Contract.DB_FIELD_FLOOR_NUM + "=" + i;
            Cursor cursor = getContentResolver().query(Contract.FLOOR_DATA_URI, null, whereClause, null, null);
            if(cursor != null && cursor.getCount() == 1) {
                cursor.moveToFirst();
                String fileName = cursor.getString(cursor.getColumnIndex(Contract.DB_FIELD_IMAGE_FILE));
                String tokens[] = fileName.split("/");
                mResNames[i].editText.setText(tokens[tokens.length - 1]);
                mResNames[i].id = cursor.getInt(cursor.getColumnIndex(Contract.DB_FIELD_ID));
            }
            if(cursor != null) {
                cursor.close();
            }
        }
    }

    private EditText addEditText(int index) {
        LinearLayout layout = new LinearLayout(this);
        layout.setOrientation(LinearLayout.HORIZONTAL);
        TextView text = new TextView(this);
        text.setText(String.format(getResources().getString(R.string.floor), index + 1));
        EditText edit = new EditText(this);
        edit.setHint(R.string.image_file);
        layout.addView(text);
        layout.addView(edit);
        mLayout.addView(layout);
        return edit;
    }
}
