package com.cs.mines.wifiservice;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.net.Uri;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class MainService extends Service {
    //these constants should be defined in a common shared contract class between client and service
    public static final int REGISTER = 0xDEAD;
    public static final int REGISTER_RSP = 0xCAFE;
    public static final int UNREGISTER = 0xBEEF;
    public static final int UNREGISTER_RSP = 0xDEAF;
    public static final int RESPONSE = 0xFEED;
    public static final int SET_POINTS = 0xBEAD;
    public static final String LATITUDE = "lat";
    public static final String LONGITUDE = "lon";
    public static final String POINTS_USED = "points_used";

    private final int PERIOD_TIME = 7000;
    private static final String TAG = "WiFiLocationService";
    Handler mHandler;
    private final Messenger mMessenger = new Messenger(mHandler = new MessageHandler());
    WifiManager mWifi;
    ArrayList<Messenger> mSubscribers;
    private Compass mCompass = null;
    private Location mCurrLoc;
    private int mApIds[];
    private int mNumPoints;

    private class Tuple {
        public int locationId;
        public double distance;
    }

    private class MessageHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            Message rsp;
            Log.d(TAG, "received message: " + msg.what);
            switch(msg.what) {
                case REGISTER:
                    Log.d(TAG, "received register request");
                    if(msg.replyTo != null && !mSubscribers.contains(msg.replyTo)) {
                        mSubscribers.add(msg.replyTo);
                    }
                    rsp = Message.obtain();
                    if(rsp != null) {
                        rsp.what = REGISTER_RSP;
                        try {
                            Log.d(TAG, "sending register response");
                            msg.replyTo.send(rsp);
                        } catch (RemoteException e) {
                            e.printStackTrace();
                            Toast.makeText(MainService.this, "Unable to respond to register request, removing", Toast.LENGTH_LONG).show();
                            mSubscribers.remove(msg.replyTo);
                        }
                    } else {
                        Toast.makeText(MainService.this, "Unable to obtain message to respond to register request", Toast.LENGTH_LONG).show();
                    }
                    break;
                case UNREGISTER:
                    Log.d(TAG, "received unregister response");
                    mSubscribers.remove(msg.replyTo);
                    rsp = Message.obtain();
                    if(rsp != null && msg.replyTo != null) {
                        rsp.what = UNREGISTER_RSP;
                        try {
                            Log.d(TAG, "sending unregister response");
                            msg.replyTo.send(rsp);
                        } catch (RemoteException e) {
                            e.printStackTrace();
                            Toast.makeText(MainService.this, "Unable to respond to unregister request", Toast.LENGTH_LONG).show();
                        }
                    } else {
                        Toast.makeText(MainService.this, "Unable to obtain message ro respond to unregister request or no messenger to reply to defined", Toast.LENGTH_LONG).show();
                    }
                    break;
                case SET_POINTS:
                    mNumPoints = msg.arg1;
                    Log.d(TAG, "setting points to: " + mNumPoints);
                default:
                    super.handleMessage(msg);
            }
        }
    }

    BroadcastReceiver mBr = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            calculatePosition();
        }
    };

    Runnable mTimer = new Runnable() {
        @Override
        public void run() {
            mHandler.postDelayed(this, PERIOD_TIME);
            mWifi.startScan();
        }
    };

    @Override
    public void onCreate() {
        mSubscribers = new ArrayList<Messenger>();
        mWifi = (WifiManager)getSystemService(Context.WIFI_SERVICE);
        mWifi.setWifiEnabled(true);
        SensorManager sm = (SensorManager)getSystemService(Context.SENSOR_SERVICE);
        if(sm.getSensorList(Sensor.TYPE_MAGNETIC_FIELD).size() > 0) {
            mCompass = new Compass(this);
            mCompass.resumeCompass();
        }
        IntentFilter filter = new IntentFilter();
        filter.addAction(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION);
        registerReceiver(mBr, filter);
        mHandler.postDelayed(mTimer, PERIOD_TIME);
        mNumPoints = 1;
    }

    @Override
    public void onDestroy() {
        mHandler.removeCallbacks(mTimer);
        unregisterReceiver(mBr);
        if(mCompass != null) {
            mCompass.pauseCompass();
            mCompass = null;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mMessenger.getBinder();
    }

    private void broadcastResults(double lat, double lon) {
        Bundle bundle = new Bundle();
        bundle.putDouble(LATITUDE, lat);
        bundle.putDouble(LONGITUDE, lon);
        bundle.putInt(POINTS_USED, mNumPoints);
        for(Messenger messenger : mSubscribers) {
            Message msg = Message.obtain();
            if(msg != null) {
                msg.what = RESPONSE;
                msg.setData(bundle);
                try {
                    Log.d(TAG, "Sending location update with values, lat: " + lat + ", lon: " + lon);
                    messenger.send(msg);
                } catch (RemoteException e) {
                    Log.d(TAG, "Unable to send response to subscriber, removing");
                }
            }
        }
    }

    //descending order comparator for scan results
    Comparator<ScanResult> mComparator = new Comparator<ScanResult>() {
        @Override
        public int compare(ScanResult a, ScanResult b) {
            return b.level - a.level;
        }
    };

    //ascending order comparator for distance tuples
    Comparator<Tuple> mTupleComparator = new Comparator<Tuple>() {
        @Override
        public int compare(Tuple a, Tuple b) {
            int retVal;
            double diff = a.distance - b.distance;
            if(diff < 0.0) {
                retVal = -1;
            } else if(diff > 0.0) {
                retVal = 1;
            } else {
                retVal = 0;
            }
            return retVal;
        }
    };

    private void calculatePosition() {
        List<ScanResult> results = mWifi.getScanResults();
        // remove hotspots we aren't interested in from the results
        for(int i = 0; i < results.size(); i++) {
            if(!results.get(i).SSID.equals("CSMwireless") && !results.get(i).SSID.equals("CSMguest")) {
                results.remove(i);
                i--;
            }
        }
        Collections.sort(results, mComparator);
        //if(results.size() > 3) {
        //    results = results.subList(0, 3);
        //}

        //Don't use nodes with values that are too small
        ArrayList<ScanResult> vals = new ArrayList<ScanResult>();
        for(ScanResult res : results) {
            if(res.level > -85) {
                vals.add(res);
            }
        }

        Cursor cursor = getRssis(vals);
        if(cursor != null) {
            List<Location> locs = getLocations(cursor);
            cursor.close();
            List<Tuple> distances = getDistances(locs);
            Collections.sort(distances, mTupleComparator);
            if(distances.size() > mNumPoints) {
                distances = distances.subList(0, mNumPoints);
            }
            getPositionAndBroadcast(distances);
        } else {
            broadcastResults(0.0, 0.0);
        }
    }

    private void getPositionAndBroadcast(List<Tuple> locations) {
        double lat = 0, lon = 0;
        Cursor cursor;
        //if we have direction info, query multiple points and average them, if we don't, only query
        //our best fit and use that straight up
        if(mComparator != null) {
            String whereClause = Contract.DB_FIELD_ID + " IN (";
            for(int i = 0; i < locations.size(); i++) {
                whereClause += locations.get(i).locationId;
                if(i < locations.size() - 1) {
                    whereClause += ", ";
                }
            }
            whereClause += ")";
            cursor = getContentResolver().query(Contract.LOCATION_URI, null, whereClause, null, null);
        } else {
            cursor = getContentResolver().query(Uri.parse(Contract.LOCATION_URI.toString() + "/" +
                    locations.get(0).locationId), null, null, null, null);
        }


        if(cursor != null && cursor.getCount() > 0) {
            for(cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                lat += cursor.getDouble(cursor.getColumnIndex(Contract.DB_FIELD_LATITUDE));
                lon += cursor.getDouble(cursor.getColumnIndex(Contract.DB_FIELD_LONGITUDE));
            }
            lat = lat / ((double)cursor.getCount());
            lon = lon / ((double)cursor.getCount());
        }
        broadcastResults(lat, lon);
    }

    private List<Tuple> getDistances(List<Location> locations) {
        ArrayList<Tuple> retVal = new ArrayList<Tuple>();
        for(Location loc : locations) {
            Tuple dist = new Tuple();
            dist.distance = loc.getDistance(mCurrLoc);
            dist.locationId = loc.getLocId();
            retVal.add(dist);
        }
        return retVal;
    }

    private List<Location> getLocations(Cursor cursor) {
        ArrayList<Location> locs = new ArrayList<Location>();
        Location loc = null;
        int currLocId = -1;
        for(cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
            if(currLocId != cursor.getInt(cursor.getColumnIndex(Contract.DB_FIELD_LOCATION))) {
                if(loc != null && loc.isValid()) {
                    locs.add(loc);
                }
                loc = new Location(mApIds, cursor.getInt(cursor.getColumnIndex(Contract.DB_FIELD_LOCATION)));
                currLocId = cursor.getInt(cursor.getColumnIndex(Contract.DB_FIELD_LOCATION));
            }
            if(loc == null) {
                loc = new Location(mApIds, cursor.getInt(cursor.getColumnIndex(Contract.DB_FIELD_LOCATION)));
            }
            loc.addRssi(cursor.getInt(cursor.getColumnIndex(Contract.DB_FIELD_LOCATION)),
                    cursor.getDouble(cursor.getColumnIndex(Contract.DB_FIELD_RSSI)),
                    cursor.getInt(cursor.getColumnIndex(Contract.DB_FIELD_ACCESS_POINT)));
        }
        if(loc != null) {
            locs.add(loc);
        }
        return locs;
    }

    private Cursor getRssis(List<ScanResult> list) {
        String whereClause = Contract.DB_FIELD_ACCESS_POINT_ADDR + " IN (";
        mApIds = null;
        for(int i = 0; i < list.size(); i++) {
            whereClause += "'";
            whereClause += list.get(i).BSSID;
            whereClause += "'";
            if(i < list.size() - 1) {
                whereClause += ", ";
            }

        }
        whereClause += ")";
        Cursor cursor = getContentResolver().query(Contract.ACCESS_POINT_URI, null, whereClause, null, null);
        if(cursor != null && cursor.getCount() > 0) {
            mApIds = new int[cursor.getCount()];
            double rssis[] = new double[mApIds.length];
            int i = 0;
            for(cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                mApIds[i] = cursor.getInt(cursor.getColumnIndex(Contract.DB_FIELD_ID));
                String bssid = cursor.getString(cursor.getColumnIndex(Contract.DB_FIELD_ACCESS_POINT_ADDR));
                for(ScanResult res : list) {
                    if(res.BSSID.equals(bssid)) {
                        rssis[i] = res.level;
                        break;
                    }
                }
                i++;
            }
            //set up our current location for later use, use a dummy location id
            mCurrLoc = new Location(mApIds, 0);
            for(i = 0; i < mApIds.length; i++) {
                mCurrLoc.addRssi(0, rssis[i], mApIds[i]);
            }
        }
        if(cursor != null) {
            cursor.close();
        }

        if(mApIds == null) {
            return null;
        }

        whereClause = Contract.DB_FIELD_ACCESS_POINT + " IN (";
        for(int i = 0; i < mApIds.length; i++) {
            whereClause += mApIds[i];
            if(i < mApIds.length - 1) {
                whereClause += ", ";
            }
        }
        whereClause += ")";
        //if we have direction info, filter on it as it gives better results
        if(mCompass != null) {
            int direction = mCompass.getCurrentDirection();
            whereClause += " AND " + Contract.DB_FIELD_DIRECTION + "=" + direction;
        }

        cursor = getContentResolver().query(Contract.RSSI_URI, null, whereClause, null, Contract.DB_FIELD_LOCATION + " ASC");
        if(cursor != null && cursor.getCount() == 0) {
            cursor.close();
            cursor = null;
        }
        return cursor;
    }
}
