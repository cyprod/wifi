package com.cs.mines.wifiservice;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class WifiProvider extends ContentProvider {
    private static final String DB_NAME = "locations.db";
    private static final String FILES_DIR = "files";
    private static final int RSSIS = 1;
    private static final int RSSI = 2;
    private static final int LOCATIONS = 3;
    private static final int LOCATION = 4;
    private static final int BUILDING_INFO = 5;
    private static final int BUILDING_INFO_W_ID = 6;
    private static final int ACCESS_POINTS = 7;
    private static final int ACCESS_POINT = 8;
    private static final int FLOORS = 9;
    private static final int FLOOR = 10;
    private static final int RAW_RSSI = 11;
    private static final int EXPORT = 12;
    private static final int REF_POINT = 13;
    private static final int REF_POINTS = 14;

    private final class DatabaseHelper extends SQLiteOpenHelper {
        public DatabaseHelper(Context context) {
            super(context, DB_NAME, null, 1);
            Log.d(TAG, "Helper Constructor");
            File db = context.getDatabasePath(DB_NAME);
            if(!db.exists()) {
                Log.d(TAG, "Copying database to " + db.getAbsolutePath());
                AssetManager am = context.getAssets();
                try {
                    this.getReadableDatabase();
                    InputStream source = am.open(DB_NAME);
                    copyFile(source, db.getAbsolutePath());
                    String[] files = am.list(FILES_DIR);
                    if(files != null) {
                        for(String file : files) {
                            source = am.open(FILES_DIR + "/" + file);
                            copyFile(source, getContext().getFilesDir().toString() + "/" + file);
                        }
                    }
                } catch(IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            Log.d(TAG, "Helper onCreate");
            db.execSQL("CREATE TABLE IF NOT EXISTS " + Contract.DB_TABLE_BUILDING_INFO + "(" +
                    Contract.DB_FIELD_ID            + " INTEGER PRIMARY KEY ASC, " +
                    Contract.DB_FIELD_BUILDING_NAME + " TEXT not null unique," +
                    Contract.DB_FIELD_NUM_FLOORS    + " INTEGER not null);");

            db.execSQL("CREATE TABLE IF NOT EXISTS " + Contract.DB_TABLE_FLOOR_INFO + "(" +
                    Contract.DB_FIELD_ID                  + " INTEGER PRIMARY KEY ASC, " +
                    Contract.DB_FIELD_BUILDING_ID         + " INTEGER REFERENCES " + Contract.DB_TABLE_BUILDING_INFO + "(" + Contract.DB_FIELD_ID + ") not null, " +
                    Contract.DB_FIELD_FLOOR_NUM           + " INTEGER not null, " +
                    Contract.DB_FIELD_IMAGE_FILE          + " TEXT not null, " +
                    Contract.DB_FIELD_IMAGE_FILE_INTERNAL + " TEXT not null, " +
                    "CONSTRAINT unq UNIQUE (" + Contract.DB_FIELD_BUILDING_ID + ", " + Contract.DB_FIELD_FLOOR_NUM + "));");

            db.execSQL("CREATE TABLE IF NOT EXISTS " + Contract.DB_TABLE_REFERENCE_POINTS + "(" +
                    Contract.DB_FIELD_ID        + " INTEGER PRIMARY KEY ASC, " +
                    Contract.DB_FIELD_FLOOR_ID  + " INTEGER REFERENCES " + Contract.DB_TABLE_FLOOR_INFO + "(" + Contract.DB_FIELD_ID + ") not null, " +
                    Contract.DB_FIELD_PIXEL_X   + " INTEGER not null, " +
                    Contract.DB_FIELD_PIXEL_Y   + " INTEGER not null, " +
                    Contract.DB_FIELD_LATITUDE  + " REAL not null, " +
                    Contract.DB_FIELD_LONGITUDE + " REAL not null, " +
                    "CONSTRAINT unq UNIQUE (" + Contract.DB_FIELD_PIXEL_X + ", " + Contract.DB_FIELD_PIXEL_Y + ", " + Contract.DB_FIELD_FLOOR_ID + "), " +
                    "CONSTRAINT unq UNIQUE (" + Contract.DB_FIELD_LATITUDE + ", " + Contract.DB_FIELD_LONGITUDE + ", " + Contract.DB_FIELD_FLOOR_ID + "));");

            db.execSQL("CREATE TABLE IF NOT EXISTS " + Contract.DB_TABLE_ACCESS_POINTS + "(" +
                    Contract.DB_FIELD_ID                + " INTEGER PRIMARY KEY ASC, " +
                    Contract.DB_FIELD_ACCESS_POINT_NAME + " TEXT not null, " +
                    Contract.DB_FIELD_ACCESS_POINT_ADDR + " TEXT not null unique);");

            db.execSQL("CREATE TABLE IF NOT EXISTS " + Contract.DB_TABLE_LOCATION_POINTS + "(" +
                    Contract.DB_FIELD_ID        + " INTEGER PRIMARY KEY ASC, " +
                    Contract.DB_FIELD_LATITUDE  + " REAL not null, " +
                    Contract.DB_FIELD_LONGITUDE + " REAL not null, " +
                    Contract.DB_FIELD_PIXEL_X + " INTEGER not null, " +
                    Contract.DB_FIELD_PIXEL_Y + " INTEGER not null, " +
                    Contract.DB_FIELD_FLOOR_ID + " INTEGER REFERENCES " + Contract.DB_TABLE_FLOOR_INFO + "(" + Contract.DB_FIELD_ID + ") not null, " +
                    "CONSTRAINT unq UNIQUE (" + Contract.DB_FIELD_PIXEL_X + ", " + Contract.DB_FIELD_PIXEL_Y + ", " + Contract.DB_FIELD_FLOOR_ID + "));");

            db.execSQL("CREATE TABLE IF NOT EXISTS " + Contract.DB_TABLE_RSSIS + "(" +
                    Contract.DB_FIELD_ID           + " INTEGER PRIMARY KEY ASC, " +
                    Contract.DB_FIELD_LOCATION     + " INTEGER REFERENCES " + Contract.DB_TABLE_LOCATION_POINTS + "(" + Contract.DB_FIELD_ID + ") not null, " +
                    Contract.DB_FIELD_ACCESS_POINT + " INTEGER REFERENCES " + Contract.DB_TABLE_ACCESS_POINTS   + "(" + Contract.DB_FIELD_ID + ") not null, " +
                    Contract.DB_FIELD_RSSI         + " REAL not null, " +
                    Contract.DB_FIELD_NUM_SAMPLES  + " INTEGER not null, " +
                    Contract.DB_FIELD_DIRECTION    + " INTEGER not null, " +
                    "CONSTRAINT unq UNIQUE (" + Contract.DB_FIELD_LOCATION + ", " + Contract.DB_FIELD_ACCESS_POINT + ", " + Contract.DB_FIELD_DIRECTION + "));");

            //this table just keeps the raw data around so we can change methods later if we decide
            //the mean being used in the "RSSIS" table should be different.
            db.execSQL("CREATE TABLE IF NOT EXISTS " + Contract.DB_TABLE_RAW_RSSIS + "(" +
                    Contract.DB_FIELD_ID + " INTEGER PRIMARY KEY ASC, " +
                    Contract.DB_FIELD_RSSI + " REAL not null, " +
                    Contract.DB_FIELD_RSSI_ID + " INTEGER REFERENCES " + Contract.DB_TABLE_RSSIS + "(" + Contract.DB_FIELD_ID + ") not null);");
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int i, int i2) {
            Log.d(TAG, "Helper onUpgrade");
            if(i != i2) {
                db.execSQL("DROP TABLE *");
                onCreate(db);
            }
        }
    }

    @Override
    public ParcelFileDescriptor openFile(Uri uri, String mode) throws FileNotFoundException{
        Log.d(TAG, "openFile");
        if(!mode.equals("r")) {
            return null;
        }
        File path = getContext().getFilesDir();
        File file = new File(path, uri.getLastPathSegment());
        return ParcelFileDescriptor.open(file, ParcelFileDescriptor.MODE_READ_ONLY);
    }

    private SQLiteDatabase mDb;
    private DatabaseHelper mDbHelper;
    private static String TAG = "WiFiDatabase";

    private static final UriMatcher sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
    static {
        sUriMatcher.addURI(Contract.AUTHORITY, Contract.DB_TABLE_RSSIS, RSSIS);
        sUriMatcher.addURI(Contract.AUTHORITY, Contract.DB_TABLE_RSSIS + "/#", RSSI);
        sUriMatcher.addURI(Contract.AUTHORITY, Contract.DB_TABLE_LOCATION_POINTS, LOCATIONS);
        sUriMatcher.addURI(Contract.AUTHORITY, Contract.DB_TABLE_LOCATION_POINTS + "/#", LOCATION);
        sUriMatcher.addURI(Contract.AUTHORITY, Contract.DB_TABLE_BUILDING_INFO, BUILDING_INFO);
        sUriMatcher.addURI(Contract.AUTHORITY, Contract.DB_TABLE_BUILDING_INFO + "/#", BUILDING_INFO_W_ID);
        sUriMatcher.addURI(Contract.AUTHORITY, Contract.DB_TABLE_ACCESS_POINTS, ACCESS_POINTS);
        sUriMatcher.addURI(Contract.AUTHORITY, Contract.DB_TABLE_ACCESS_POINTS + "/#", ACCESS_POINT);
        sUriMatcher.addURI(Contract.AUTHORITY, Contract.DB_TABLE_FLOOR_INFO, FLOORS);
        sUriMatcher.addURI(Contract.AUTHORITY, Contract.DB_TABLE_FLOOR_INFO + "/#", FLOOR);
        sUriMatcher.addURI(Contract.AUTHORITY, Contract.DB_TABLE_RAW_RSSIS, RAW_RSSI);
        sUriMatcher.addURI(Contract.AUTHORITY, Contract.DB_EXPORT + "/*", EXPORT);
        sUriMatcher.addURI(Contract.AUTHORITY, Contract.DB_TABLE_REFERENCE_POINTS + "/#", REF_POINT);
        sUriMatcher.addURI(Contract.AUTHORITY, Contract.DB_TABLE_REFERENCE_POINTS, REF_POINTS);
    }

    @Override
    public boolean onCreate() {
        Log.d(TAG, "onCreate");
        //populate database if it exists
        mDbHelper = new DatabaseHelper(getContext());
        mDb = mDbHelper.getWritableDatabase();
        return false;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        Log.d(TAG, "query");
        SQLiteQueryBuilder query = new SQLiteQueryBuilder();
        int val = sUriMatcher.match(uri);
        switch(val) {
            case RSSI:
                query.appendWhere(Contract.DB_FIELD_ID + "=" + uri.getLastPathSegment());
            case RSSIS:
                query.setTables(Contract.DB_TABLE_RSSIS);
                break;
            case LOCATION:
                query.appendWhere(Contract.DB_FIELD_ID + "=" + uri.getLastPathSegment());
            case LOCATIONS:
                query.setTables(Contract.DB_TABLE_LOCATION_POINTS);
                break;
            case ACCESS_POINT:
                query.appendWhere(Contract.DB_FIELD_ID + "=" + uri.getLastPathSegment());
            case ACCESS_POINTS:
                query.setTables(Contract.DB_TABLE_ACCESS_POINTS);
                break;
            case FLOOR:
                query.appendWhere(Contract.DB_FIELD_ID + "=" + uri.getLastPathSegment());
            case FLOORS:
                query.setTables(Contract.DB_TABLE_FLOOR_INFO);
                break;
            case BUILDING_INFO_W_ID:
                query.appendWhere(Contract.DB_FIELD_ID + "=" + uri.getLastPathSegment());
            case BUILDING_INFO:
                query.setTables(Contract.DB_TABLE_BUILDING_INFO);
                break;
            case REF_POINT:
                query.appendWhere(Contract.DB_FIELD_ID + "=" + uri.getLastPathSegment());
            case REF_POINTS:
                query.setTables(Contract.DB_TABLE_REFERENCE_POINTS);
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri + ", match code: " + val);
        }
        Cursor cursor = query.query(mDb, projection, selection, selectionArgs, null, null, sortOrder);
        if(cursor != null) {
            cursor.setNotificationUri(getContext().getContentResolver(), uri);
        }
        return cursor;
    }

    @Override
    public String getType(Uri uri) {
        Log.d(TAG, "getType");
        return null;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        Log.d(TAG, "insert");
        String table;
        boolean insertImage = false;
        int val = sUriMatcher.match(uri);
        switch(val) {
            case RSSIS:
                table = Contract.DB_TABLE_RSSIS;
                break;
            case LOCATIONS:
                table = Contract.DB_TABLE_LOCATION_POINTS;
                break;
            case ACCESS_POINTS:
                table = Contract.DB_TABLE_ACCESS_POINTS;
                break;
            case BUILDING_INFO:
                table = Contract.DB_TABLE_BUILDING_INFO;
                break;
            case FLOORS:
                insertImage = true;
                values.put(Contract.DB_FIELD_IMAGE_FILE_INTERNAL, "TEMP");
                table = Contract.DB_TABLE_FLOOR_INFO;
                break;
            case RAW_RSSI:
                table = Contract.DB_TABLE_RAW_RSSIS;
                break;
            case REF_POINTS:
                table = Contract.DB_TABLE_REFERENCE_POINTS;
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri + ", match code: " + val);
        }
        long id = mDb.insert(table, null, values);
        if(insertImage) {
            try {
                String internalFile = importImage(values.getAsString(Contract.DB_FIELD_IMAGE_FILE),
                        values.getAsInteger(Contract.DB_FIELD_BUILDING_ID),
                        values.getAsInteger(Contract.DB_FIELD_FLOOR_NUM), null);
                if(internalFile != null) {
                    values.remove(Contract.DB_FIELD_IMAGE_FILE_INTERNAL);
                    values.put(Contract.DB_FIELD_IMAGE_FILE_INTERNAL, internalFile);
                    mDb.update(table, values, Contract.DB_FIELD_ID + "=" + id, null);
                } else {
                    throw new IllegalArgumentException("Error inserting image file");
                }
            } catch(IllegalArgumentException e) {
                mDb.delete(table, Contract.DB_FIELD_ID + "=" + id, null);
                throw e;
            }
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return Uri.parse(uri + "/" + id);
    }

    private String importImage(String newFileString, int buildingId, int floorId, String previousFileString) {
        if(newFileString == null) {
            if(previousFileString == null) {
                throw new IllegalArgumentException("No floor image provided, no existing file");
            }
            return null;
        }
        File newFile = new File(newFileString);
        //old file name matches new file name, and old file exists but new file doesn't exist, so keep old file
        if(newFileString.equals(previousFileString) && !newFile.exists()) {
            return null;
        }
        if(newFile.exists()) {
            String internalFileString = getContext().getFilesDir() + "/" +buildingId + "_" + floorId + ".dat";
            copy(newFileString, internalFileString);
            File internalFile = new File(internalFileString);
            if(internalFile.exists()) {
                internalFile.setReadable(true, false);
                return internalFileString;
            } else {
                throw new IllegalArgumentException("Error creating internal file");
            }
        } else {
            throw new IllegalArgumentException("Supplied file does not exist");
        }
    }

    @Override
    public int delete(Uri uri, String s, String[] strings) {
        Log.d(TAG, "delete");
        String table;
        switch(sUriMatcher.match(uri)) {
            case REF_POINT:
                table = Contract.DB_TABLE_REFERENCE_POINTS;
                s = Contract.DB_FIELD_ID + "=" + uri.getLastPathSegment();
                strings = null;
                break;
            default:
                throw new IllegalArgumentException("Can't delete URI '" + uri.toString() + "'");
        }
        mDb.delete(table, s, strings);
        return 0;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        Log.d(TAG, "update");
        String table;
        int rowsUpdated;
        switch(sUriMatcher.match(uri)) {
            case RSSI:
                table = Contract.DB_TABLE_RSSIS;
                break;
            case FLOOR:
                if(values.containsKey(Contract.DB_FIELD_IMAGE_FILE)) {
                    Cursor cursor = mDb.query(Contract.DB_TABLE_FLOOR_INFO, null,
                            Contract.DB_FIELD_ID + "=" + uri.getLastPathSegment(), null, null, null, null);
                    if(cursor != null && cursor.getCount() == 1) {
                        cursor.moveToFirst();
                        String internalFile = importImage(values.getAsString(Contract.DB_FIELD_IMAGE_FILE),
                                cursor.getInt(cursor.getColumnIndex(Contract.DB_FIELD_BUILDING_ID)),
                                cursor.getInt(cursor.getColumnIndex(Contract.DB_FIELD_FLOOR_NUM)),
                                cursor.getString(cursor.getColumnIndex(Contract.DB_FIELD_IMAGE_FILE)));
                        if(internalFile != null) {
                            values.put(Contract.DB_FIELD_IMAGE_FILE_INTERNAL, internalFile);
                        }
                    }
                    if(cursor != null) {
                        cursor.close();
                    }
                }
                table = Contract.DB_TABLE_FLOOR_INFO;
                break;
            case LOCATION:
                table = Contract.DB_TABLE_LOCATION_POINTS;
                break;
            case EXPORT:
                if(!Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
                    return Contract.MEDIA_NOT_MOUNTED;
                }
                return exportToFile(Environment.getExternalStorageDirectory() + "/" + uri.getLastPathSegment());
            default:
                throw new IllegalArgumentException("Cannot update uri: " + uri);
        }
        rowsUpdated = mDb.update(table, values, Contract.DB_FIELD_ID + "=" + uri.getLastPathSegment(), null);
        getContext().getContentResolver().notifyChange(uri, null);
        return rowsUpdated;
    }

    private int exportToFile(String destination) {
        File file = new File(destination);
        if(!deleteDir(file.getAbsolutePath())) {
            return -1;
        }
        if(!file.mkdir()) {
            return -1;
        }
        File imageDir = new File(file, FILES_DIR);
        if(!imageDir.mkdir()) {
            return -1;
        }
        int retVal = copy(mDb.getPath(), destination + "/" + DB_NAME);
        if(retVal == Contract.SUCCESS) {
            int numNotCopied = copyDir(getContext().getFilesDir().getAbsolutePath(), destination);
            if(numNotCopied != 0) {
                retVal = numNotCopied;
            }
        }
        mDb = mDbHelper.getWritableDatabase();
        return retVal;
    }

    private int copy(String src, String dest) {
        FileInputStream source;
        try {
            source = new FileInputStream(src);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return Contract.NO_SOURCE_DB;
        }
        return copyFile(source, dest);
    }

    private int copyFile(InputStream src, String dest) {
        int retVal = Contract.SUCCESS;
        OutputStream out = null;
        try {
            out = new FileOutputStream(dest);

            byte[] buf = new byte[1024];
            int len;
            while((len = src.read(buf)) > 0) {
                out.write(buf, 0, len);
            }
        } catch (IOException e) {
            e.printStackTrace();
            retVal = Contract.CANT_COPY;
        } finally {
            try {
                if(src != null) {
                    src.close();
                }
            } catch(IOException e) {
                e.printStackTrace();
            }
            try {
                if(out != null) {
                    out.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return retVal;
    }

    private boolean deleteDir(String path) {
        File file = new File(path);
        if(!file.exists()) {
            return true;
        }
        if(!file.isDirectory()) {
            return file.delete();
        }
        File[] files = file.listFiles();
        if(files != null) {
            for(File subFile : files) {
                if(!deleteDir(subFile.getAbsolutePath())) {
                    return false;
                }
            }
        }
        return file.delete();
    }

    private int copyDir(String src, String destFolder) {
        int numNotCopied = 0;
        File srcFile = new File(src);
        if(!srcFile.exists()) {
            Log.e(TAG, "File does not exist: '" + srcFile.getAbsoluteFile().toString() + "'");
            return 1;
        }
        if(!srcFile.isDirectory()) {
            File destFile = new File(destFolder, srcFile.getName());
            if(copy(srcFile.getAbsolutePath(), destFile.getAbsolutePath()) != Contract.SUCCESS) {
                Log.e(TAG, "Error coping file: '" + srcFile.getAbsoluteFile() + ";");
                return 1;
            }
        }
        File newDestFolder = new File(destFolder, srcFile.getName());
        if(!newDestFolder.exists() && !newDestFolder.mkdir()) {
            Log.e(TAG, "Error creating directory: '" + newDestFolder.toString() + "'");
            return 1;
        }
        File[] files = srcFile.listFiles();
        if(files != null) {
            for(File subFile : files) {
                File newSrc = new File(src, subFile.getName());
                numNotCopied += copyDir(newSrc.getAbsolutePath(), newDestFolder.getAbsolutePath());
            }
        }
        return numNotCopied;
    }
}
