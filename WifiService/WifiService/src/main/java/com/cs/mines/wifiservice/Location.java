package com.cs.mines.wifiservice;

public class Location {
    private static final double INVALID_RSSI = 1000.0; //so big that it'd cook people so will never get a valid value this big
    private final int mAps[];
    private double mRssis[];
    private final int mLocId;

    public Location(int aps[], int locId) {
        mAps = aps;
        mLocId = locId;
        mRssis = new double[mAps.length];
        for(int i = 0; i < mRssis.length; i++) {
            mRssis[i] = INVALID_RSSI;
        }
    }

    public int getLocId() {
        return mLocId;
    }

    public double getDistance(Location loc) {
        double squares = 0;
        for(int i = 0; i < loc.mRssis.length; i++) {
            squares += Math.pow(mRssis[i] - loc.mRssis[i], 2);
        }
        return Math.sqrt(squares);
    }

    public boolean isValid() {
        boolean retVal = true;
        for(double rssi : mRssis) {
            if(rssi == INVALID_RSSI) {
                retVal = false;
            }
        }
        return retVal;
    }

    public void addRssi(int locId, double rssi, int ap) {
        if(locId == mLocId) {
            for(int i = 0; i < mAps.length; i++) {
                if(mAps[i] == ap) {
                    mRssis[i] = rssi;
                }
            }
        }
    }




}
