package com.cs.mines.wifiservice;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

public class Compass implements SensorEventListener{
    private static final int NORTH = 0;
    private static final int EAST = 1;
    private static final int SOUTH = 2;
    private static final int WEST = 3;

    private SensorManager mSM;
    private int mDirection;

    public Compass(Context context) {
        mSM = (SensorManager)context.getSystemService(Context.SENSOR_SERVICE);
    }

    public void pauseCompass() {
        mSM.unregisterListener(this);
    }

    public void resumeCompass() {
        Sensor gSensor = mSM.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        Sensor mSensor = mSM.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
        mSM.registerListener(this, gSensor, SensorManager.SENSOR_DELAY_NORMAL);
        mSM.registerListener(this, mSensor, SensorManager.SENSOR_DELAY_NORMAL);
    }

    //variables used for calculating direction info
    private float[] mGData = new float[3];
    private float[] mMdata = new float[3];
    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        float[] data;
        float[] R = new float[16];
        float[] I = new float[16];
        float[] orientation = new float[3];
        switch(sensorEvent.sensor.getType()) {
            case Sensor.TYPE_ACCELEROMETER:
                data = mGData;
                break;
            case Sensor.TYPE_MAGNETIC_FIELD:
                data = mMdata;
                break;
            default:
                return;
        }
        System.arraycopy(sensorEvent.values, 0, data, 0, data.length);

        SensorManager.getRotationMatrix(R, I, mGData, mMdata);
        SensorManager.getOrientation(R, orientation);
        double direction = Math.toDegrees((double)orientation[0]);


        if(direction >= -45 && direction < 45) {
            mDirection = NORTH;
        } else if(direction >= 45 && direction < 135) {
            mDirection = EAST;
        } else if(direction >= -135 && direction < -45) {
            mDirection = WEST;
        } else {
            mDirection = SOUTH;
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }

    public int getCurrentDirection() {
        return mDirection;
    }
}