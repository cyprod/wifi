package com.cs.mines.wifites;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Date;

public class MainActivity extends Activity {
    //these constants should be defined in a common shared contract class between client and service
    public static final int REGISTER = 0xDEAD;
    public static final int REGISTER_RSP = 0xCAFE;
    public static final int UNREGISTER = 0xBEEF;
    public static final int UNREGISTER_RSP = 0xDEAF;
    public static final int RESPONSE = 0xFEED;
    public static final int SET_POINTS = 0xBEAD;
    public static final String LATITUDE = "lat";
    public static final String LONGITUDE = "lon";
    public static final String POINTS_USED = "points_used";
    private static final String TAG = "WifiTester";

    private Messenger mService = null;
    private Messenger mMessenger;
    private boolean mBound = false, mWasBound, mStateChanging;
    private TextView mLat, mLon;
    private Button mButton, mSetPoints, mCapturePoint;
    private EditText mPointsText, mDescription;
    private MyHandler mHandler;
    private int mLastNumPoints = -1;
    private double mLastLat= 0.0, mLastLon = 0.0;

    private class MyHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            Log.d(TAG, "received a message: " + msg.what);
            switch(msg.what) {
                case RESPONSE:
                    Log.d(TAG, "Got location update");
                    Bundle data = msg.getData();
                    if(data != null) {
                        DecimalFormat format = new DecimalFormat("0.000000");
                        mLat.setText(format.format(data.getDouble(LATITUDE, 0.0)));
                        mLon.setText(format.format(data.getDouble(LONGITUDE, 0.0)));
                        mLastNumPoints = data.getInt(POINTS_USED, -1);
                        mLastLat = data.getDouble(LATITUDE, 0.0);
                        mLastLon = data.getDouble(LONGITUDE, 0.0);
                    }
                    break;
                case REGISTER_RSP:
                    Log.d(TAG, "Got register response");
                    mStateChanging = false;
                    mButton.setText(R.string.disconnect);
                    break;
                case UNREGISTER_RSP:
                    Log.d(TAG, "Got unregister response");
                    unbindService(mConnection);
                    mService = null;
                    mBound = false;
                    mStateChanging = false;
                    mButton.setText(R.string.connect);
                    break;
                default:
                    super.handleMessage(msg);
            }
        }
    }

    private ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            mService = new Messenger(iBinder);
            mBound = true;
            Message msg = mHandler.obtainMessage(REGISTER);
            msg.replyTo = mMessenger;
            boolean success = false;
            if(msg != null) {
                try {
                    Log.d(TAG, "Sent register request");
                    mService.send(msg);
                    success = true;
                    Toast.makeText(MainActivity.this, "Established Service Connection", Toast.LENGTH_LONG).show();
                } catch (RemoteException e) {
                    e.printStackTrace();
                    Toast.makeText(MainActivity.this, "Error sending subscribe message", Toast.LENGTH_LONG).show();
                }
            }
            if(!success) {
                Toast.makeText(MainActivity.this, "Error obtaining message", Toast.LENGTH_LONG).show();
                unbindService(mConnection);
                mBound = false;
                mStateChanging = false;
                mButton.setText(R.string.connect);
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            mService = null;
            mBound = false;
            mStateChanging = false;
            mButton.setText(R.string.connect);
            Toast.makeText(MainActivity.this, "Lost Service Connection", Toast.LENGTH_LONG).show();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mWasBound = mStateChanging = false;
        mLat = (TextView)findViewById(R.id.textLat);
        mLon = (TextView)findViewById(R.id.textLon);
        mButton = (Button)findViewById(R.id.button);
        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeBindState();
            }
        });
        mSetPoints = (Button)findViewById(R.id.buttonSetPoints);
        mSetPoints.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setPoint();
            }
        });
        mCapturePoint = (Button)findViewById(R.id.buttonCapture);
        mCapturePoint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                capturePoint();
            }
        });
        mPointsText = (EditText)findViewById(R.id.editText);
        mDescription = (EditText)findViewById(R.id.editText3);
        mHandler = new MyHandler();
        mMessenger = new Messenger(mHandler);
    }

    private void setPoint() {
        int points = Integer.parseInt(mPointsText.getText().toString());
        Message msg = mHandler.obtainMessage(SET_POINTS);
        msg.arg1 = points;
        try {
            mService.send(msg);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    private void capturePoint() {
        File file = new File(Environment.getExternalStorageDirectory(), "locationLog.txt");
        try {
            BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(file, true));
            String line = new Date().toString() + ": Description-" + mDescription.getText() + " Lat-" + mLastLat + " Lon-" + mLastLon + " numPoints-" + mLastNumPoints + "\n";
            stream.write(line.getBytes());
            stream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void changeBindState() {
        if(!mStateChanging) {
            if(!mBound) {
                mButton.setText(R.string.connecting);
                mStateChanging = true;
                Intent intent = new Intent("com.cs.mines.service.WiFiService");
                bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
            } else {
                mButton.setText(R.string.disconnecting);
                mStateChanging = true;
                Message msg = mHandler.obtainMessage(UNREGISTER);
                boolean success = false;
                if(msg != null) {
                    msg.replyTo = mMessenger;
                    try {
                        Log.d(TAG, "Sent unregister message");
                        mService.send(msg);
                        success = true;
                    } catch (RemoteException e) {
                        e.printStackTrace();
                        Toast.makeText(this, "Error sending unsubscribe message", Toast.LENGTH_LONG).show();
                    }
                }
                if(!success) {
                    //if error, just force an unsubscribe
                    mStateChanging = false;
                    mBound = false;
                    mButton.setText(R.string.connect);
                    unbindService(mConnection);
                    mService = null;
                }
            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        if(mWasBound) {
            mStateChanging = false;
            changeBindState();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();

        mWasBound = mBound;
        if(mBound) {
            mStateChanging = false;
            changeBindState();
        }
    }
}
